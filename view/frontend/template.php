<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?= $title ?></title>
        <link rel="stylesheet" href="../../public/lib/bootstrap/css/bootstrap.min.css" />
        <link href="../../public/css/style.css" rel="stylesheet" />
    </head>

    <body>

        <?= $content ?>


        <script src="../../public/lib/jquery/jquery.min.js"></script>
        <script src="../../public/lib/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>