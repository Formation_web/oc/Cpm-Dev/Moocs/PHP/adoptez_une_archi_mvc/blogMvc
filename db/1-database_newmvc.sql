--
-- Base de données :  `newmvc`
--
create database if not exists newmvc character set utf8 collate utf8_unicode_ci;

use newmvc;

grant all privileges on newmvc.* to 'newmvc_user'@'localhost' identified by 'secret';

-- --------------------------------------------------------