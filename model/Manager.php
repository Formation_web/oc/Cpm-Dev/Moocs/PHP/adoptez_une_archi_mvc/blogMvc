<?php

namespace OpenClassrooms\Blog\Model;


abstract class Manager
{
	protected function dbConnect()
	{
		try {
			$db = new \PDO('mysql:host=localhost;dbname=newMvc;charset=utf8', 'newmvc_user', 'secret');
			return $db;
		} catch (Exception $e) {
			throw new Exception('Impossible de se connecter à ma base de données');
		}
	}
}



