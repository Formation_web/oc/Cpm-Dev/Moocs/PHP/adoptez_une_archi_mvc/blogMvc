<?php


require_once('model/PostManager.php');
require_once('model/CommentManager.php');



function listPosts()
{
	$postManager = new \OpenClassrooms\Blog\Model\PostManager();
	$posts = $postManager->getPosts();

	require('view/frontend/listPostsView.php');
}

function post()
{
	$postManager = new \OpenClassrooms\Blog\Model\PostManager();
	$commentManager = new \OpenClassrooms\Blog\Model\CommentManager();

	$post = $postManager->getPost($_GET['id']);
	$comments = $commentManager->getComments($_GET['id']);

	require('view/frontend/postView.php');
}

function update($commentId)
{
	$commentManager = new \OpenClassrooms\Blog\Model\CommentManager();

	$comment = $commentManager->getComment($_GET['id']);

	require('view/frontend/updateCommentView.php');
}

function addComment($postId, $author, $comment)
{
	$commentManager = new \OpenClassrooms\Blog\Model\CommentManager();

	$affectedLines = $commentManager->postComment($postId, $author, $comment);

	if ($affectedLines === false)
	{
		// ERREUR gérées. Elle sera remonté jusq'au bloc try{...} du router
		throw new Exception('Impossible d\'ajouter le commentaire !!!');
	}
	else
	{
		header('Location: index.php?action=post&id=' . $postId);
	}
}

function modifyComment($id, $author, $comment)
{
	$commentManager = new \OpenClassrooms\Blog\Model\CommentManager();

	$affectedLines = $commentManager->updateComment($id, $author, $comment);


	if ($affectedLines === false)
	{
		// ERREUR gérées. Elle sera remonté jusq'au bloc try{...} du router
		throw new Exception('Impossible de modifier le commentaire !!!');
	}
	else
	{
		header('Location: index.php?action=update&id=' . $id);
	}
}